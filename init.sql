CREATE DATABASE IF NOT EXISTS foo;
USE foo;
CREATE TABLE IF NOT EXISTS bar(charvar VARCHAR(255) DEFAULT "hello world");
INSERT INTO 'bar' VALUES ('hello world');
