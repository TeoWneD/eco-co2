# Questions
## Generales 
* Cites une chose que tu as appris recemment.
* Comment tu te tiens a jour ?
* Qu'est ce qui t'interresses dans la partie Ops du boulot ?
* Quels outils installerais tu le premier jour? (OS, editeur, packages...)
## Systeme
* Expliques quels sont les etapes de demarrage d'un systeme Linux en quelques mots.
* Quels sont les outils que tu utilises pour identifier un process ?
* Comment listerais tu les derniers fichiers modifies sur le systeme ?
* Cites deux facons de recharger la configuration d'un service.
## Networing
* Le serveur A ne communique plus avec le serveur B, expliques ta demarche de resolution.
* Que sais tu du modele OSI ?
* Comment deploie tu du code actuellement ?

# Tests pratique
## Docker
### Compose 
* Produire un docker-compose pour creer les images suivantes:
  * Une image mysql avec un compte toto et avec mot de passe titi. Le mot de passe root sera randomise. La base "foo" et sa table "bar" avec un seul champ charvar contenant "hello world" sera cree et packagee dans l'image.
  * Une image php avec nginx ou apache, delivrant sur le port 8080 un simple "hello world" lu depuis une table de la BDD. Le fichier php sera integre dans l'image.
 
### CI
* Produire un gitlab-ci.yml pour la construction des images en local (le runner peut etre la machine du candidat, voir la doc de gitlab)

## Ansible
* Produire un playbook qui permette, avec le fichier d'inventaire "hosts.yml":
  * Installer ou mettre a jour ntpd sur la machine locale
  * Configurer le service pour prendre comme serveurs de temps 0.debian.pool.ntp.org
  * Recharger le service apres configuration
* Fournir la commande permettant de l'executer

## Reseau
* Comment organiserais tu pour une application:
  * Deux reseaux prives, un pour les instances de travail et l'autre pour les serveurs de BDD
  * Un proxy loadbalancer
  * Trois serveurs de travail
  * Deux serveurs de BDD
  * Autant de switches, routeurs, parefeux et IP publique que necessaire

