FROM mysql
MAINTAINER  teo.sudarovich@gmail.com

COPY init.sql /db/init.sql

WORKDIR db

COPY ./init.sql /docker-entrypoint-initdb.d/
#CMD mysql -u $MYSQL_USER -p $MYSQL_PASSWORD $MYSQL_DATABASE < db/init.sql
